;
(function () { // ; protect from concatenation

    'use strict'; // string?! makes global (window) variable overrides an error http://goo.gl/8wdR8h

    angular// look at John Papa's pattern to move functions to named variables vs. inline

        .module('socialNotes', ['ngRoute'])

        .config(function ($routeProvider) { // can only inject providers and constants

            $routeProvider

                .when('/controllers', {
                    templateUrl: 'templates/controllersExample.html', // case matters (not URL)
                    // template: '<div>test</div>', // instead of templateUrl, to test
                    controller: 'controllersExampleController'
                })

                .otherwise('/controllers')
            ;

        })

        .controller('controllersExampleController', function ($scope) {

            $scope.name = 'Jon';

        })

    ; // setter version requires [] else getter

})()
; // isolate the use strict to function

;
(function () {
})
;