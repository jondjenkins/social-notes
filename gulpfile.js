// this is node code

var gulp = require('gulp'),
    watch = require('gulp-watch'),
    plumber = require('gulp-plumber'),
    connect = require('gulp-connect'); // connect is a small web server

gulp.task('connect', function () {

    connect.server({
        root: 'www',
        livereload: true // refresh any browsers looking at server when updated
    });

});

gulp.task('watch', function() {

    gulp.watch(['www/*.html'], ['html']); // watch all html files and run html task

});

gulp.task('html', function() {

    gulp.src('www/*.html').pipe(connect.reload()); // kick server, tell to reload

});

gulp.task('default', ['connect', 'watch']); // default gulp task(s) to perform